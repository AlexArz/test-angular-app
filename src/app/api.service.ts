import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export type User = {
  id: number;
  name: string;
  username: string;
  email: string;
  address: Address;
  phone: string;
  website: string;
  company: Company;
};

export type Todo = {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
};

export type Address = {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: {
    lat: string;
    lng: string;
  };
};

export type Company = {
  name: string;
  catchPhrase: string;
  bs: string;
};

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public state;
  constructor(private http: HttpClient) {}

  getUsers(): Observable<Array<User>> {
    return this.http.get<Array<User>>(
      'https://jsonplaceholder.typicode.com/users'
    );
  }

  getToDo(id: number): Observable<Array<Todo>> {
    return this.http.get<Array<Todo>>(
      `https://jsonplaceholder.typicode.com/todos?userId=${id}`
    );
  }
}
