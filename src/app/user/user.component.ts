import { ApiService, Todo, User } from './../api.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit, OnDestroy {
  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private router: Router
  ) {}

  public user: User;
  public todos: Todo[] = [];
  public sub1$: Subscription;
  public sub2$: Subscription;

  ngOnInit(): void {
    this.route.data.subscribe((data) => {});
    this.sub1$ = this.apiService.getUsers().subscribe((res) => {
      const users = res;
      this.route.params.subscribe((user) => {
        this.user = users.find((el) => el.id === +user.id);
        if (!this.user) {
          this.router.navigate(['/user']);
        }
      });
    });

    this.route.params.subscribe((user) => {
      this.sub2$ = this.apiService.getUsers().subscribe((res) => {
        const users = res;
        this.user = users.find((el) => el.id === +user.id);
      });

      this.apiService.getToDo(+user.id).subscribe((todo) => {
        for (let i = 0; i < todo.length; i++) {
          if (todo[i].completed) {
            this.todos.push(todo[i]);
          } else {
            this.todos.unshift(todo[i]);
          }
        }
      });
    });
  }

  ngOnDestroy() {
    this.sub1$.unsubscribe();
    this.sub2$.unsubscribe();
  }
}
