import { ApiService } from './../api.service';
import {
  Component,
  ViewChild,
  OnInit,
  OnDestroy,
  AfterViewInit,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {
  public dataSource;
  public form: FormGroup;
  public sub: Subscription;
  public state = {
    companies: [],
    selectedCompany: '',
    pageSize: null,
    pageIndex: null,
    sortActive: 'name',
    sortDirection: 'asc',
    selectedColumn: 'name',
  };

  @ViewChild('paginator', { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(public apiService: ApiService) {}

  displayedColumns: string[] = [
    'avatar',
    'name',
    'username',
    'email',
    'address',
  ];

  columnsForSort: string[] = ['name', 'username', 'email', 'address'];

  ngOnInit() {
    const state = this.apiService.state;
    this.form = new FormGroup({
      select: new FormControl(''),
    });
    if (state) {
      this.form.controls.select.setValue(state.selectedCompany);
      this.dataSource = state.dataSource;
      this.state = {
        companies: state.companies,
        selectedCompany: state.selectedCompany,
        pageSize: this.dataSource.paginator.pageSize,
        pageIndex: this.dataSource.paginator.pageIndex,
        sortActive: this.dataSource.sort.active,
        sortDirection: this.dataSource.sort.direction,
        selectedColumn: this.dataSource.sort.active,
      };
    } else {
      this.sub = this.apiService.getUsers().subscribe((res) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        res.forEach((el) => {
          this.state.companies.push(el.company.name);
        });
      });
    }
  }

  ngAfterViewInit() {
    if (this.apiService.state) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  applyFilter(ev) {
    this.state.selectedCompany = ev.value;
    this.dataSource.filterPredicate = (data: any, filter: string) =>
      data.company.name === filter;
    this.dataSource.filter = ev.value;
  }

  clear() {
    this.state.selectedCompany = '';
    this.dataSource.filter = null;
    this.form.reset();
  }

  sortSelect() {
    const sortState: Sort = {
      active: this.state.selectedColumn,
      direction: 'asc',
    };
    this.sort.active = sortState.active;
    this.sort.direction = sortState.direction;
    this.sort.sortChange.emit(sortState);
  }

  sortChange(ev) {
    this.state.selectedColumn = ev.active;
  }

  ngOnDestroy() {
    this.apiService.state = {
      selectedCompany: this.state.selectedCompany,
      dataSource: this.dataSource,
      companies: this.state.companies,
    };
    this.sub.unsubscribe();
  }
}
